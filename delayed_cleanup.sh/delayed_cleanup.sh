# Example script name: fetch
clean_fetch() {
   # Delayed cleanup
   if test -z "${FETCH_NO_CLEAN}" ;
   then
      nohup /bin/bash <<EOF 1>/dev/null 2>&1 &
sleep "${FETCH_CLEANUP_SEC:-300}" ; /bin/rm -r "${FETCH_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

trap "__ec=$? ; clean_fetch ; trap '' {0..20} ; exit ${__ec} ;" {0..20}
FETCH_TMPDIR="$( mktemp -d )"
tmpfile1="$( TMPDIR="${FETCH_TMPDIR}" mktemp )"
tmpfile2="$( TMPDIR="${FETCH_TMPDIR}" mktemp )"