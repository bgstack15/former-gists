@echo off
REM Filename: Map.bat
REM Location: %Data%
REM Author: bgstack15
REM Startdate: 2013-03-26
REM Title: Map Drive Script
REM Purpose: Reads a config file and maps drives
REM History: 130326 map.bat started with code copied from shares.bat
REM          130326.165446 reads CSV and works.
REM          130326.170749 provides usage and also accepts filename to load
REM          130326.183604 Version 1.0 complete
REM          130330.205031 Version 1.1 fixed ping for host-up test
REM          130523.101231 Version 1.2 added optional volume label
REM          130716.162420 Version 1.3 Provides some error checking.
REM References:
REM     http://tools.ietf.org/html/rfc4180
REM         CSV standard
REM     http://ss64.com/nt/setlocal.html
REM     http://stackoverflow.com/questions/3713601/subroutines-in-batch-files
REM         refers to subroutines and passing/returning data
REM     http://stackoverflow.com/questions/8481558/windows-batch-goto-within-if-block-behaves-very-strangely
REM         refers to calling multiple subroutines
REM     http://www.robvanderwoude.com/ntfortokens.php
REM     http://ss64.com/nt/for_f.html
REM         refers to for tokens and delims
REM     http://stackoverflow.com/questions/2541767/what-is-the-proper-way-to-test-is-variable-is-empty-in-a-batch-file-if-not-1
REM         if string not null
REM     http://stackoverflow.com/questions/9329749/batch-errorlevel-ping-response
REM         is a host online
REM     http://oreilly.com/pub/h/1105
REM         location of registry key for network share volume labels
REM     http://stackoverflow.com/questions/5488071/cmd-regedit-from-cmd
REM     http://ss64.com/nt/reg.html
REM         regedit from cmd
REM     http://stackoverflow.com/questions/7525711/dos-batchfile-is-skipping-for-delims
REM         batch for skipping consecutive delims
REM	    http://www.robvanderwoude.com/redirection.php
REM         output redirection
REM     http://stackoverflow.com/questions/8438511/if-or-if-in-a-windows-batch-file
REM         batch if or
REM     http://au.answers.yahoo.com/question/index?qid=20120710195214AAWGSpw
REM         command-line windows credentials
REM     http://hardforum.com/showthread.php?t=1320763
REM         Same question as me: cancel prompt about username, password...
REM     http://stackoverflow.com/questions/3088712/errorlevel-of-command-executed-by-batch-for-loop/6658935#6658935

setlocal EnableDelayedExpansion

:dispUsage
if "%1"=="/?" (
    echo Maps network drives as specified by a CSV file.
    echo.
    echo %0 [filename]
    echo    filename    Specifies which file to load. Default is map.csv.
    echo.
    echo An example CSV file includes the following:
    echo.
    echo    driveletter,sharename,username,password,vollabel
    echo    SERVER,server
    echo    w,vol1,username,password,w_vol1
    echo    x,vol2
    echo    SERVER,192.168.100.24
    echo    y,sharename,,,y_share
    echo    z,mydrive,username,password
    echo.
    echo Technical details:
    echo The first line of the CSV is skipped for the purpose of headers.  Any
    echo filename called must exist or Map will load the default file.
    echo IP addresses may be used in the place of hostnames.  Share names may include
    echo spaces because the data are comma-separated.
    goto :almosteof
)

set filename=map.csv
:getfile
if not [%1]==[] (
    if exist "%1" (
        set filename=%1
    )
)

:restartPoint

set /a servergood=0
set /a counter=0

net use * /delete

set "comma=,"
for /f "skip=1 delims=" %%l in (%filename%) do (
	set "line=%%l"
	set "line=#!line:%comma%=%comma%#!"
	for /f "tokens=1,2,3,4,5 delims=," %%a in ("!line!") do (
		if /i "#SERVER"=="%%a" (
			set servername=%%b
			set "servername=!servername:~1,99!"
			call :subPingServer
		) else (
			REM not server, so
			REM echo %%a %%b %%c %%d %%e
			set letter=%%a
			set "letter=!letter:~1!"
			set sharename=%%b
			set "sharename=!sharename:~1!"
			set username=%%c
			set "username=!username:~1!"
			set password=%%d
			set "password=!password:~1!"
			set vollabel=%%e
			set "vollabel=!vollabel:~1!"
			call :subMap
		)
	)
)

goto :almosteof
REM the above line ends the program because there are no more CALLs in history

:subPingServer
set /a servergood=0
ping %servername% -n 1|find "(0%">nul && set /a servergood=1
echo %servername% is %servergood%

:afterserver
exit /b
REM the above line operates as a "return" function

:subMap
REM %letter% %sharename% %username% %password% %vollabel%
if "%servergood%"=="1" (
    set "TRUE="
	if [%password%]==[~1] set TRUE=1 
	if [%password%]==[] set TRUE=1
	del dela delb /f /q 2>nul 1>nul
	if defined TRUE	(
        echo |net use %letter%: "\\%servername%\%sharename%" /persistent:YES 2>delb 1>dela
		REM this part is the cool part: the "echo |" passes "" to the net use in case it prompts for username/password.
		REM When the net use gets that input on the username prompt, it returns "user canceled the action" and we
		REM capture that later on.
    ) else (
        echo |net use %letter%: "\\%servername%\%sharename%" %password% /USER:%username% /persistent:YES 2>&1 1>dela
    )
	findstr /r /i /c:"successfully" dela 1>nul 2>nul
	if [!errorlevel!]==[0] (
        echo Success: %letter%: as \\%servername%\%sharename%
		set "FALSE=1"
		if [%vollabel%]==[~1] set FALSE=
		if [%vollabel%]==[] set FALSE=
		if defined FALSE (
			reg add "HKCU\Software\Microsoft\Windows\CurrentVersion\Explorer\MountPoints2\##%servername%#%sharename:\=#%" /v "_LabelFromReg" /d "%vollabel%" /f 2>&1 > nul
		)
    ) else (
	    echo Failed: %letter%: as \\%servername%\%sharename%.
		
		REM possible errors:
		REM 1. syserr 55: "no longer available" = folder doesn't exist.     ACTIONS: inform user, continue
		REM 2.      1223: "canceled" = wrong password/username.             ACTIONS: ask for creds, ask to re-run script.
		REM 3. syserr  3: "cannot find the path specified" = bad sharename. ACTIONS: inform user, continue
		REM 4. syserr 85: "already in use" = didn't remove old drive maps.  ACTIONS: warn user, continue
		::type delb
		::echo above is delb.
		set thisErr=
		findstr /r /i /c:"cannot find the path specified" delb 1>nul 2>nul
		if [!errorlevel!]==[0] set thisErr=notexists
		findstr /r /i /c:"longer" delb 1>nul 2>nul
		if [!errorlevel!]==[0] set thisErr=nolongeravailable
		findstr /r /i /c:"canceled" delb 1>nul 2>nul
		if [!errorlevel!]==[0] set thisErr=wrongcreds
		findstr /r /i /c:"already in use" delb 1>nul 2>nul
		if [!errorlevel!]==[0] set thisErr=alreadyused
		if [!thisErr!]==[wrongcreds] (
			echo Cannot connect to the server due to incorrect credentials...
			REM echo true=%TRUE%
			if defined TRUE (
				set /p uname=Enter the username to connect to '%servername%': 
				REM echo !uname!
				cmdkey /add:%servername% /u:!uname! /p
				REM cmdkey /add:%servername% /user:%username% /pass:%password%
			) else (
				cmdkey /add:%servername% /u:%username% /p:%password%
			)
			::echo Break the script and try again, now that the password is added to Windows Credential vault.
			::pause
			echo Restarting script...
			goto :restartPoint
			exit /b
			REM let's see how that works...
		)
		if [!thisErr!]==[nolongeravailable] (
			echo This resource no longer exists. Skipping this drive.
		)
		if [!thisErr!]==[notexists] (
			echo This resource does not exist. Skipping this drive.
		)
		if [!thisErr!]==[alreadyused] (
			echo This drive letter is already in use. Skipping this drive.
		)
    )
)

:skipMap
exit /b
REM the above line operates as a "return" function

:almosteof
REM clean up just in case we left these behind
del dela delb /f /q