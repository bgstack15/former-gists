#!/usr/bin/env python
# Filename: get-my-gists.py
# Location: gitlab, probably
# Author: Chris Arndt (stackoverflow uid 39275), bgstack15
# Startdate: 2018-06-05 21:49
# Title: Script That Downloads GitHub Gists in a Nice Format
# Purpose: To facilitate my departure from GitHub
# History:
# Usage: ./get-my-gists.py bgstack15
# Reference:
#    copied from https://stackoverflow.com/questions/6724490/pull-all-gists-from-github/34052242#34052242
# Improve:
# -*- coding: utf-8 -*-
"""Clone all gists of GitHub username given on the command line."""

import subprocess
import sys
import requests

if len(sys.argv) > 1:
   gh_user = sys.argv[1]
else:
   print("Usage: get-my-gists.py <GitHub username>")
   sys.exit(1)

req = requests.get('https://api.github.com/users/%s/gists' % gh_user)

for gist in req.json():

   # get attributes
   name = gist['files'].keys()[0]
   descrip = gist['description']

   # debugging
   print name + ": " + descrip

   # clone the repo
   ret = subprocess.call(['git', 'clone', gist['git_pull_url'], name])
   if ret != 0:
      print("ERROR cloning gist %s. Please check output." % gist['id'])

   # save description
   with open(name + "/" + "description", "w") as text_file:
      text_file.write(descrip)
