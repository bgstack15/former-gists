#!/bin/sh
# File: /etc/ansible/books/stable/clone/dependencies/sapsnug.sh
# Author: bgstack15
# Startdate: 2018-04-05 10:12
# Title: Script to Accept Parameters to Send to Nsupdate Using Gsstsig
# Purpose: To wrap nsupdate -g in ansible
# History:
# Usage:
#    Run as root, or define variables SNUG_PASSWORD and SNUG_USERNAME
# Variables:
#    SNUG_DEBUG with any value will direct output to cat instead of nsupdate -g.
#    SNUG_DELIM="%" will replace this character with newlines, which nsupdate uses as statement delimiters
# Examples:
#    SNUG_PASSWORD="example" SNUG_USERNAME="bgstack15" ./sapsnug.sh "update add 12.20.200.10.in-addr.arpa 300 IN PTR clonetest212.prod1.example.com."
# Reference:
#    original research
#    learn if root user framework.sh
#    just use stdin for kinit https://serverfault.com/questions/422778/how-to-automate-kinit-process-to-obtain-tgt-for-kerberos/422783#422783
# Improve:
# Documentation:
#    This script exists because the nsupdate for ptr to our AD only works with gsstsig.
#    Execute this manually with:
#    kinit -E -k "$( hostname -s | tr '[[:lower:]]' '[[:upper:]]' )$"
#    # DELETE ENTRY, single session
#    nsupdate <<'EOF'
#    update delete clonetest212.prod1.example.com. A
#    send
#    gsstsig
#    update delete 12.20.200.10.in-addr.arpa ptr
#    send
#    EOF
#    # CREATE ENTRY, single session
#    nsupdate <<'EOF'
#    update add clonetest212.prod1.example.com. 86400 A 10.200.20.12
#    send
#    gsstsig
#    update add 12.20.200.10.in-addr.arpa 300 IN PTR clonetest212.prod1.example.com.
#    send
#    EOF

# Define functions
fail_out() {
   # call: fail_out 1 "this will leave now with rc 1."
   local trc="${1}" ; shift
   test -n "${@}" && printf "%s\n" "${@}" 1>&2
   exit "${trc:-1}"
}

# learn if root user
test "${USER}" = "root" && is_root=1
test -n "${SUDO_USER}" && is_root="sudo"

# prepare to get kerberos ticket
if test -z "${is_root}" ;
then
   # not root, so need to use environment variables
   test -z "${SNUG_USERNAME}" && fail_out 1 "${0}: SNUG_USERNAME is not defined, or was not run as root. Aborted."
   test -z "${SNUG_PASSWORD}" && fail_out 1 "${0}: SNUG_PASSWORD is not defined, or was not run as root. Aborted."
fi

# get kerberos ticket
if test -n "${is_root}" ;
then
   # do it with host kerberos ticket
   /bin/kinit -E -k "$( hostname -s | tr '[[:lower:]]' '[[:upper:]]' )$"
else
   # do it with username and password
   SNUG_USERNAME="$( printf "%s" "${SNUG_USERNAME}" | sed -r -e 's/^.*\\//;' -e 's/@.*$//;' )"
   printf "%s" "${SNUG_PASSWORD}" | /bin/kinit "${SNUG_USERNAME}" 1>/dev/null ; trc=$?
   case "${trc}" in
      0) : ;;
      *) fail_out "${trc}" "${0}: kinit exited unexpectedly with code ${trc}. Aborted." ;;
   esac
fi

# fail out if klist fails
/usr/bin/klist 1>/dev/null 2>&1 ; trc=$?
case "${trc}" in
   0) : ;;
   *) fail_out "${trc}" "${0}: klist exited unexpectedly with code ${trc}. Aborted." ;;
esac

# run nsupdate with the commands
{
   printf "%s\n" "$@" | sed -r -e "s/${SNUG_DELIM:-%}\s?/\n/g;"
   echo "send"
   echo "quit"
} | {
   if test -n "${SNUG_DEBUG}" ;
   then
      cat - ; trc=$?
   else
      nsupdate -g ; trc=$?
   fi
}

exit "${trc}"