#!/bin/sh
# File: /mnt/public/www/smith122/repo/devuan-deb/newest-package-for-devuan.sh
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2019-11-29 21:28
# Title: Script that Manipulates Dependencies for a Binary Dpkg
# Purpose: Download latest version of python3-ipalib and modify it for internal use
# History:
# Usage:
#    /mnt/public/www/smith122/repo/devuan-deb/
#    Next steps include running update-devuan-deb.sh to update the apt repo.
# Reference:
#    Original research
#    dpkg-architecture --list-known
# Improve:
# Dependencies:
#    curl, dpkg-deb, sed, grep

# FUNCTIONS
#customize_package "${PACKAGE}" "${DEPENDS_REMOVE}" "${DEPENDS_ADD}" "${SOURCE_DIR}" "${OUT_DIR}"
customize_package() {
   # weaknesses: this might struggle with adding or removing "foobar (>= 0.1.1~bz2)" or other complex version numbers.
   ___cp_package="${1}"
   ___cp_depends_remove="${2}"
   ___cp_depends_add="${3}"
   ___cp_source_dir="${4}"
   ___cp_out_dir="${5}"

   # Learn latest version to fetch
   ___cp_newfile="$( curl -L -s "${___cp_source_dir}" | grep -oE ">${___cp_package}.*<\/a" | sed -r -e 's/^>//;' -e 's/<\/a$//;' )"
   echo "Discovered filename ${___cp_newfile}"
   curl -L -s "${___cp_source_dir}/${___cp_newfile}" -o "${___cp_newfile}" 1>/dev/null 2>&1
   ___cp_tmpdir="temp_${$}_deb"
   mkdir -p "${___cp_tmpdir}"
   dpkg-deb -R "${___cp_newfile}" "${___cp_tmpdir}"

   # Remove the requested dependencies
   echo "${___cp_depends_remove}" | tr ',' '\n' | while read word ;
   do
      if test "${word}" != "" ;
      then
         echo "Removing dependency ${word}"
         sed -i -r -e "s/[:,]\s+${word}[^,]{0,13}(,)?/\1/;" "${___cp_tmpdir}/DEBIAN/control"
      fi
   done

   # Add the requested dependencies
   echo "${___cp_depends_add}" | tr ',' '\n' | while read word ;
   do
      if test "${word}" != "" ;
      then
         echo "Adding dependency ${word}"
         sed -i -r -e "/^Depends:/s/$/, ${word}/" "${___cp_tmpdir}/DEBIAN/control"
      fi
   done
   # Remove trailing comma, just in case
   sed -i -r -e '/^Depends:/s/,\s*$//;' "${___cp_tmpdir}/DEBIAN/control"

   # Calculate new file name
   ___cp_newfile2="$( echo "${___cp_newfile}" | sed -r -e 's/((_(amd64|i386|all))?\..{1,6})$/+stackrpms\1/;' )"
   dpkg-deb -b "${___cp_tmpdir}" "${___cp_newfile2}"

   # Move to outdir
   test -e "${___cp_newfile2}" && mv "${___cp_newfile2}" "${___cp_out_dir}/"

   # Clean up
   rm -rf "${___cp_tmpdir}" "${___cp_newfile}"
}

PACKAGE=python3-ipalib
DEPENDS_REMOVE="systemd"
DEPENDS_ADD=""
SOURCE_DIR="http://ftp.us.debian.org/debian/pool/main/f/freeipa"
OUT_DIR="/mnt/public/www/smith122/repo/devuan-deb/p"
customize_package "${PACKAGE}" "${DEPENDS_REMOVE}" "${DEPENDS_ADD}" "${SOURCE_DIR}" "${OUT_DIR}"
