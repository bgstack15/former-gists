#!/bin/sh
# Filename: fetch-all-album-images.sh
# Location: 
# Author: bgstack15@gmail.com
# Startdate: 2018-07-06 06:57:34
# Title: Script That Automatically Downloads and Names all Album Images for a URL at AllCDCovers.Com
# Purpose: To automate getting the album art for CDs
# Package: 
# History: 
# Usage: 
#    ./fetch-all-album-images.sh https://www.allcdcovers.com/show/7070/hilary_duff_dignity_2007_retail_cd/front
# Reference: ftemplate.sh 2018-06-21a; framework.sh 2017-11-11a
# Improve:
fiversion="2018-06-21a"
fetchallalbumimagesversion="2018-07-06a"

usage() {
   ${PAGER:-/usr/bin/less -F} >&2 <<ENDUSAGE
usage: fetch-all-album-images.sh [-duV] [-c conffile] [-o <outdir>] <url1>
This script fetches all the album cover images available on allcdcovers.com for the requested URL
version ${fetchallalbumimagesversion}
 -d debug   Show debugging info, including parsed variables.
 -u usage   Show this usage block.
 -V version Show script version number.
 -c conf    Read in this config file.
 -o outdir  Sets FAAI_OUTDIR
 <url1>     One of the URLs for the album in question on allcdcovers.com
Return values:
 0 Normal
 1 Help or version info displayed
 2 Count or type of flaglessvals is incorrect
 3 Incorrect OS type
 4 Unable to find dependency
 5 Not run as root or sudo
 6 Webpage error
Environment variables:
FAAI_OUTDIR  Destination directory for downloaded images. Default is PWD.
ENDUSAGE
}

# DEFINE FUNCTIONS

operate_on_url() {
   # call: operate_on_url "{turl}"
   local turl="${1}"
   debuglev 9 && ferror "operate_on_url $@"

   # learn baseurl
   local baseurl="${turl%/*}"
   # fetch webpage
   local tp="$( curl -H "${FAAI_USERAGENT}" -c "${cookiefile}" -s -k -L "${turl}" )"

   # parse values
   local this_artist="$( echo "${tp}" | awk '/<title>Download .* - .*<\/title>/{$1="";print}' | sed -r -e 's/^\s*//' -e 's/ - .*$//;' )"
   local this_album="$( echo "${tp}" | awk "/<h1>${this_artist} - .*<\/h1>/{print}" | sed -r -e 's/.* - //;' -e 's/<\/h[0-9]>.*$//;' )"
   this_album="$( filter_album_title "${this_album}" )"

   # learn all available images
   local this_section="$( echo "${tp}" | sed -n -r -e "/<h2>Other/,/<\/h3>/p" )"
   local these_items="$( echo "${this_section}" | grep -E 'class="thumbnail"' | grep -oE '\/show\/[A-Za-z0-9\/_]+">' | sed -r -e 's/.*\///;' -e 's/">\s*$//;' ; echo "${turl##*/}" )"

   # display info
   echo "baseurl=${baseurl}"
   echo "artist=${this_artist}"
   echo "album=${this_album}"
   echo "items=$( echo "${these_items}" | tr '\n' ',' )"

   for this_item in ${these_items} ;
   do
      fetch_image_for "${baseurl}" "${this_item}" "${this_artist}" "${this_album}" "${turl}"
   done

}

filter_album_title() {
   # call: this_album="$( filter_album_title "${this_album}" )"
   debuglev 9 && ferror "filter_album_title $@"

   local ta="${1}"

   # remove "Retail CD" and "(2009)" years from titles
   echo "${ta}" | sed -r -e 's/\s*Retail CD//;' -e 's/\s*\([12][0-9]{3}\)//;' 
}

fetch_image_for() {
   # call: fetch_image_for "${baseurl}" "{this_item}" "${this_artist}" "${this_album}" "${turl}"
   debuglev 9 && ferror "fetch_image_for $@"

   local baseurl="${1}"
   local item="${2}"
   local artist="${3}"
   local album="${4}"
   local referer="${5}"

   local thisdomain="$( echo "${baseurl}" | grep -oE 'https?:\/\/[a-zA-Z.]*\/' )"

   local tp="$( curl -H "${FAAI_USERAGENT}" -H "${FAAI_H1}" -H "${FAAI_H2}" -H "${FAAI_H3}" --referer "${referer}" -c "${cookiefile}" -s -L -k "${baseurl}/${item}" )"

   local method=""
   echo "${tp}" | grep -qE "Captcha_image\".*action=\"\/download\/.*download\.gif" && method="captcha"
   echo "${tp}" | grep -qoE 'a href="\/.*download\.gif' && method="href"

   debuglev 5 && echo "${tp}"| grep -oE 'a href="\/.*download\.gif'

   case "${method}" in

      href)
         # fetch image
         for word in $( echo "${tp}" | awk '/class="selectedCoverThumb"/{print;}' | grep -oE 'a href="\/download\/[A-Za-z0-9\/_-]*"><img' | sed -r -e 's/^a href="\///;' -e 's/"><img.*$//;' -e "s@^@${thisdomain}@;" ) ;
         do
            echo "curl -H "${FAAI_USERAGENT}" -H ${FAAI_H1} -H ${FAAI_H2} -H ${FAAI_H3} --referer ${referer} -c ${cookiefile} -s -L -k ${word} > ${FAAI:-.}/${artist} - ${album} - ${item}.jpg"
            curl -H "${FAAI_USERAGENT}" -H "${FAAI_H1}" -H "${FAAI_H2}" -H "${FAAI_H3}" --referer "${referer}" -c "${cookiefile}" -s -L -k ${word} > "${FAAI:-.}/${artist} - ${album} - ${item}.jpg"
         done
         ;;

      captcha)
         # need to visit each page, collect its captcha action string and plug it in as a URL
         for word in $( echo "${tp}" | grep -oE '"Captcha_image".*action="\/download\/[A-Za-z0-9\/_-]*"><input type=' | sed -r -e 's/^"Captcha_image".*action="//;' -e 's/"><input type=.*$//;' -e "s@^@${thisdomain}@;" ) ;
         do
            echo "curl -H "${FAAI_USERAGENT}" -H ${FAAI_H1} -H ${FAAI_H2} -H ${FAAI_H3} --referer ${referer} -c ${cookiefile} -s -L -k ${word} > ${FAAI:-.}/${artist} - ${album} - ${item}.jpg"
            curl -H "${FAAI_USERAGENT}" -H "${FAAI_H1}" -H "${FAAI_H2}" -H "${FAAI_H3}" --referer "${referer}" -c "${cookiefile}" -s -L -k ${word} > "${FAAI:-.}/${artist} - ${album} - ${item}.jpg"
         done
         ;;

      *)
         ferror "${scripttrim} 6: Unsupported webpage provided. Output follows."
         echo "${tp}" 1>&2
         exit 2
         ;;

   esac
}

# DEFINE TRAPS

clean_fetchallalbumimages() {
   # use at end of entire script if you need to clean up tmpfiles
   # rm -f "${tmpfile1}" "${tmpfile2}" 2>/dev/null

   # Delayed cleanup
   if test -z "${FETCH_NO_CLEAN}" ;
   then
      nohup /bin/bash <<EOF 1>/dev/null 2>&1 &
sleep "${fetchallalbumimages_CLEANUP_SEC:-300}" ; /bin/rm -r "${fetchallalbumimages_TMPDIR:-NOTHINGTODELETE}" 1>/dev/null 2>&1 ;
EOF
   fi
}

CTRLC() {
   # use with: trap "CTRLC" 2
   # useful for controlling the ctrl+c keystroke
   :
}

CTRLZ() {
   # use with: trap "CTRLZ" 18
   # useful for controlling the ctrl+z keystroke
   :
}

parseFlag() {
   flag="$1"
   hasval=0
   case ${flag} in
      # INSERT FLAGS HERE
      "d" | "debug" | "DEBUG" | "dd" ) setdebug; ferror "debug level ${debug}" __debug_set_by_param=1;;
      "u" | "usage" | "help" | "h" ) usage; exit 1;;
      "V" | "fcheck" | "version" ) ferror "${scriptfile} version ${fetchallalbumimagesversion}"; exit 1;;
      #"i" | "infile" | "inputfile" ) getval; infile1=${tempval};;
      "c" | "conf" | "conffile" | "config" ) getval; conffile="${tempval}";;
      "o" | "outdir" | "out-dir" ) getval; FAAI_OUTDIR="${tempval}";;
   esac
   
   debuglev 10 && { test ${hasval} -eq 1 && ferror "flag: ${flag} = ${tempval}" || ferror "flag: ${flag}"; }
}

# DETERMINE LOCATION OF FRAMEWORK
f_needed=20171111
while read flocation ; do if test -e ${flocation} ; then __thisfver="$( sh ${flocation} --fcheck 2>/dev/null )" ; if test ${__thisfver} -ge ${f_needed} ; then frameworkscript="${flocation}" ; break; else printf "Obsolete: %s %s\n" "${flocation}" "${__this_fver}" 1>&2 ; fi ; fi ; done <<EOFLOCATIONS
./framework.sh
${scriptdir}/framework.sh
$HOME/bin/bgscripts/framework.sh
$HOME/bin/framework.sh
$HOME/bgscripts/framework.sh
$HOME/framework.sh
/usr/local/bin/bgscripts/framework.sh
/usr/local/bin/framework.sh
/usr/bin/bgscripts/framework.sh
/usr/bin/framework.sh
/bin/bgscripts/framework.sh
/usr/local/share/bgscripts/framework.sh
/usr/share/bgscripts/framework.sh
EOFLOCATIONS
test -z "${frameworkscript}" && echo "$0: framework not found. Aborted." 1>&2 && exit 4

# INITIALIZE VARIABLES
# variables set in framework:
# today server thistty scriptdir scriptfile scripttrim
# is_cronjob stdin_piped stdout_piped stderr_piped sendsh sendopts
. ${frameworkscript} || echo "$0: framework did not run properly. Continuing..." 1>&2
infile1=
outfile1=
logfile=${scriptdir}/${scripttrim}.${today}.out
define_if_new interestedparties "bgstack15@gmail.com"
# SIMPLECONF
define_if_new default_conffile "/etc/fetchallalbumimages/fetchallalbumimages.conf"
define_if_new defuser_conffile ~/.config/fetchallalbumimages/fetchallalbumimages.conf
define_if_new fetchallalbumimages_TMPDIR "$( mktemp -d )"
define_if_new FAAI_USERAGENT "Mozilla/5.0 (X11; Fedora; Linux x86_64; rv:60.0) Gecko/20100101 Firefox/60.0"
define_if_new FAAI_H1 "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8"
define_if_new FAAI_H2 "Accept-Language: en-US,en;q=0.5"
cookiefile="$( TMPDIR="${fetchallalbumimages_TMPDIR}" mktemp )"
#tmpfile2="$( TMPDIR="${fetchallalbumimages_TMPDIR}" mktemp )"

# REACT TO OPERATING SYSTEM TYPE
case $( uname -s ) in
   Linux) : ;;
   FreeBSD) : ;;
   *) echo "${scriptfile}: 3. Indeterminate OS: $( uname -s )" 1>&2 && exit 3;;
esac

## REACT TO ROOT STATUS
#case ${is_root} in
#   1) # proper root
#      : ;;
#   sudo) # sudo to root
#      : ;;
#   "") # not root at all
#      #ferror "${scriptfile}: 5. Please run as root or sudo. Aborted."
#      #exit 5
#      :
#      ;;
#esac

# SET CUSTOM SCRIPT AND VALUES
#setval 1 sendsh sendopts<<EOFSENDSH     # if $1="1" then setvalout="critical-fail" on failure
#/usr/local/share/bgscripts/send.sh -hs  # setvalout maybe be "fail" otherwise
#/usr/share/bgscripts/send.sh -hs        # on success, setvalout="valid-sendsh"
#/usr/local/bin/send.sh -hs
#/usr/bin/mail -s
#EOFSENDSH
#test "${setvalout}" = "critical-fail" && ferror "${scriptfile}: 4. mailer not found. Aborted." && exit 4

# VALIDATE PARAMETERS
# objects before the dash are options, which get filled with the optvals
# to debug flags, use option DEBUG. Variables set in framework: fallopts
validateparams - "$@"

# LEARN EX_DEBUG
test -z "${__debug_set_by_param}" && fisnum "${FAAI_DEBUG}" && debug="${FAAI_DEBUG}"

# CONFIRM TOTAL NUMBER OF FLAGLESSVALS IS CORRECT
#if test ${thiscount} -lt 2;
#then
#   ferror "${scriptfile}: 2. Fewer than 2 flaglessvals. Aborted."
#   exit 2
#fi

## LOAD CONFIG FROM SIMPLECONF
## This section follows a simple hierarchy of precedence, with first being used:
##    1. parameters and flags
##    2. environment
##    3. config file
##    4. default user config: ~/.config/script/script.conf
##    5. default config: /etc/script/script.conf
#if test -f "${conffile}";
#then
#   get_conf "${conffile}"
#else
#   if test "${conffile}" = "${default_conffile}" || test "${conffile}" = "${defuser_conffile}"; then :; else test -n "${conffile}" && ferror "${scriptfile}: Ignoring conf file which is not found: ${conffile}."; fi
#fi
#test -f "${defuser_conffile}" && get_conf "${defuser_conffile}"
#test -f "${default_conffile}" && get_conf "${default_conffile}"

# CONFIGURE VARIABLES AFTER PARAMETERS

## START READ CONFIG FILE TEMPLATE
#oIFS="${IFS}"; IFS="$( printf '\n' )"
#infiledata=$( ${sed} ':loop;/^\/\*/{s/.//;:ccom;s,^.[^*]*,,;/^$/n;/^\*\//{s/..//;bloop;};bccom;}' "${infile1}") #the crazy sed removes c style multiline comments
#IFS="${oIFS}"; infilelines=$( echo "${infiledata}" | wc -l )
#{ echo "${infiledata}"; echo "ENDOFFILE"; } | {
#   while read line; do
#   # the crazy sed removes leading and trailing whitespace, blank lines, and comments
#   if test ! "${line}" = "ENDOFFILE";
#   then
#      line=$( echo "${line}" | sed -e 's/^\s*//;s/\s*$//;/^[#$]/d;s/\s*[^\]#.*$//;' )
#      if test -n "${line}";
#      then
#         debuglev 8 && ferror "line=\"${line}\""
#         if echo "${line}" | grep -qiE "\[.*\]";
#         then
#            # new zone
#            zone=$( echo "${line}" | tr -d '[]' )
#            debuglev 7 && ferror "zone=${zone}"
#         else
#            # directive
#            varname=$( echo "${line}" | awk -F= '{print $1}' )
#            varval=$( echo "${line}" | awk -F= '{$1=""; printf "%s", $0}' | sed 's/^ //;' )
#            debuglev 7 && ferror "${zone}${varname}=\"${varval}\""
#            # simple define variable
#            eval "${zone}${varname}=\${varval}"
#         fi
#         ## this part is untested
#         #read -p "Please type something here:" response < ${thistty}
#         #echo "${response}"
#      fi
#   else

## REACT TO BEING A CRONJOB
#if test ${is_cronjob} -eq 1;
#then
#   :
#else
#   :
#fi

# SET TRAPS
#trap "CTRLC" 2
#trap "CTRLZ" 18
trap "__ec=$? ; clean_fetchallalbumimages ; trap '' 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 ; exit ${__ec} ;" 0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20

# DEBUG SIMPLECONF
debuglev 5 && {
   ferror "Using values"
   # used values: EX_(OPT1|OPT2|VERBOSE)
   set | grep -iE "^FAAI_" 1>&2
}

# MAIN LOOP
#{
   x=0
   while test ${x} -lt ${thiscount} ;
   do
      x=$(( x + 1 ))
      eval turl="\${opt${x}}"
      debuglev 5 && ferror "turl=${turl}"
      operate_on_url "${turl}"
   done
#} | tee -a ${logfile}

# EMAIL LOGFILE
#${sendsh} ${sendopts} "${server} ${scriptfile} out" ${logfile} ${interestedparties}

## STOP THE READ CONFIG FILE
#exit 0
#fi; done; }
