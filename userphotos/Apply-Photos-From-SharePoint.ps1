# File: Apply-Photos-From-SharePoint.ps1
# Locations: \\util201\scripts\Associate Photos\test\
# Author: bgstack15
# Startdate: 2019-11-15
# Title: Script that Downloads photos from Sharepoint and uploads them to destination locations
# Purpose: Pull images down from Sharepoint
# History:
# Usage:
# References:
# Improve:
# Dependencies:
#    \\util201\scripts\Functions\userphotolib.ps1
#    \\util201\scripts\Functions\loglib.ps1

# MAIN

# Load libraries
. \\util201\scripts\Functions\userphotolib.ps1

#Config Parameters
$SiteURL = "https://exampleinc.sharepoint.com/sites/UserPhotos"
$LibraryName = "Approved"
$MoveToLibraryName = "Processed"
$Outdir = "\\util201\e$\scripts\Associate Photos\test\Approved"
$MoveToOutdir = "$Outdir\..\Processed"
$ErrorOutdir = "$Outdir\..\Unidentified"
$global:logfile = "$Outdir\..\log\ap-${global:today}.log"
$global:logid = Get-Random

Log "Start Apply-Photos-From-SharePoint"

#Get Credentials to connect; customized
if (! $Cred) {$Cred = Get-Shared-Credential -User "ServiceAccount@${global:domain}" -PasswordCategory "O365"}

$null = Download-Library-Files-To-Local -SiteURL $SiteURL -LibraryName $LibraryName -Credential $Cred -Outdir $Outdir -MoveToLibraryName $MoveToLibraryName

if ($true -eq $true)
{
	# OPEN SESSIONS
	#$null = Open-Connection-AzureAD # not needed because we do not need to manually update azuread
	$null = Import-PSSession ($session = Open-Connection-Outlook) -AllowClobber

# Iterate over each file in $Outdir

	# Exclude the "username_80.jpg" filenames and the timestamp files
	ForEach ($file in Get-ChildItem $Outdir | Where-Object { -Not ( $_.Name -Match ".*(_[0-9]{2}|[0-9]{6})\.[^.]{1,6}" ) } )
	{
		$null = Process-User-Photo -File $file
	}

	# CLOSE SESSIONS
	#Close-Connection-AzureAD
	Close-Connection-Outlook $session
}

Log "End Apply-Photos-From-SharePoint"
