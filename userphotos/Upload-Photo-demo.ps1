# Filename: \\util201\scripts\Associate Photos\test\Upload-Photo-demo.ps1
# License: CC-BY-SA 4.0
# Author: bgstack15
# Startdate: 2019-11-07 16:11
# Title: Upload-Photo
# Purpose: To demonstrate how to use the User Photo library
# History: 
#    2019-11-08.04 Complete session management and photo upload/download for AD, AzureAD, and Outlook
# Usage:
# Reference:
#    https://richardjgreen.net/set-thumbnailphoto-active-directory-powershell/
#    __Resize function https://seanonit.wordpress.com/2014/11/11/understanding-byte-arrays-in-powershell/
#    A variant of the previous https://www.lewisroberts.com/2015/01/18/powershell-image-resize-function/
#    Filename manipulation https://www.reddit.com/r/PowerShell/comments/3ro9ow/removing_the_end_of_a_filename_with_powershell/
#    https://devblogs.microsoft.com/scripting/weekend-scripter-exporting-and-importing-photos-in-active-directory/
#    Azure AD takes 500kb photos https://support.microsoft.com/en-us/help/3062745/user-photos-aren-t-synced-from-the-on-premises-environment-to-exchange
# Improve:
# Documentation

. \\util201\scripts\Functions\userphotolib.ps1

# almost works; a permission isusue?
#Set-Photo-AzureAD -User bgstack15 -Filename "E:\test\thomas-magnum.PNG"

# OPEN SESSIONS
$null = Open-Connection-AzureAD
$null = Import-PSSession ($session = Open-Connection-Outlook) -AllowClobber

# MAIN
#Set-Photo-All -User "bgstack15" -Filename "E:\test\Notre Dame.jpg"

Set-Photo-All -User "bgstack15" -Filename "E:\test\thomas-magnum.PNG"
#Get-Photo-All -User "bgstack15" -Filename "E:\test\out.jpg"

# CLOSE SESSIONS
Close-Connection-AzureAD
Close-Connection-Outlook $session
