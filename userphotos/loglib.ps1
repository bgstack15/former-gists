# Filename: \\util201\scripts\Functions\loglib.ps1
# License: CC-BY-SA 4.0
# Author: bgstack15, TAFKAC
# Startdate: 2019-11-25 10:19
# Title: Logging library
# Purpose: Provide logging functions
# History: 
# Usage:
# Reference:
#    internal file systems-engineering/powershell/deploy-server/iad/library/functions.ps1
# Improve:
# Dependencies:
#

### GLOBAL VARIABLES
$global:today = Get-Date -Format yyyy-MM-dd

### FUNCTIONS
Function Log {
	# adapted from TAFKAC
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $TRUE, Position = 0)]
        [string] $entry
    ) 
    If (!($GLOBAL:log -ne $NULL)) {
        Set-Variable -Name log -Value @() -Scope Global;
    }
    $stamp = (Get-Date -Format FileDateTimeUniversal);
    $GLOBAL:log += "[$stamp] $entry";
    Write-Host "[$stamp] $entry" -ForegroundColor Gray
	if ($global:logfile -ne $null)
	{
		$logid = ""; if ($global:logid -ne $null) {$logid = " ${global:logid}"}
		"[$stamp] ${env:UserName}@${env:ComputerName}${logid}: $entry" | Out-File -Append $global:logfile
	}
}
