# File: Apply-Photos-From-SharePoint.ps1
# Locations: \\util201\scripts\Associate Photos\test\
# Author: bgstack15
# Startdate: 2019-12-04
# Title: Script that Downloads photos from AD and uploads to SQLServer
# Purpose:
# History:
# Usage:
# References:
#    Refactored to generate array first based on https://stackoverflow.com/questions/27259665/powershell-combine-get-aduser-results
# Improve:
# Dependencies:
#    \\util201\scripts\Functions\userphotolib.ps1
#    \\util201\scripts\Functions\loglib.ps1

# MAIN

# Load libraries
. \\util201\scripts\Functions\userphotolib.ps1

#Config Parameters
$Outdir = "\\util201\e$\scripts\Associate Photos\test\upload-to-sql"
$global:logfile = "$Outdir\..\log\sync-ad-to-sql-${global:today}.log"
$global:logid = Get-Random
$PermittedChanges = 10000
Remove-Variable ErrorMessage -ErrorAction SilentlyContinue

Write-Host "${GLOBAL:Logfile} is the logfile"
Log "Start Sync-AD-to-SQLServer"

if ($true -eq $true)
{
	$Changes = 0
	
	# List all users that have a thumbnailphoto
	$Users = @()
	$Users += Get-ADUser -Filter { thumbnailphoto -Like '*' } -Properties sAMAccountName | Sort-Object -Property sAMAccountName

	# Iterate over each user
	Try
	{
		ForEach ($thisUser in $Users)
		{
			if ( $Changes -Ge $PermittedChanges )
			{
				$ErrorMessage = "Reached maximum amount of changes for this execution: $Changes"
				Log "ERROR: $ErrorMessage"
				break
			} else {
				$outfile="${Outdir}\" + $thisUser.samaccountname + ".jpg"
				Get-Photo-AD -User $thisUser.samaccountname -Filename "$outfile"
				$result = Set-Photo-SQLServer -User $thisUser.samaccountname -Filename "$outfile"
				if ($result -Eq 0){$Changes += 1}
				Remove-Item -Path "$outfile"
			}
		}
	}
	Catch
	{
		if ($_.Exception.Message.Contains("System error."))
		#if ($true -eq $false)
		{
			# suppress the breakexception
		}
		else
		{
			Throw $_
		}

	}
}

Log "Made $Changes changes."
Log "End Sync-AD-to-SQLServer"
If ($ErrorMessage -ne $null) { Throw $ErrorMessage }
